<?php

/**
 * Plugin Name:       Toy Embed
 * Plugin URI:        https://gitlab.com/67656e65726963/toy-embed
 * Description:       Upsize default width (640) and reformat HTML output for embeds.
 * Version:           0.0.0
 * Author:            67656e65726963
 * Author URI:        https://gitlab.com/67656e65726963
 * License:           Unlicense
 * License URI:       http://unlicense.org/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_filter(
	'embed_defaults',
	function() {
		// Size up
		return array(
			'width'  => 640,
			'height' => 480,
		);
	},
	67
);

add_filter(
	'embed_oembed_html',
	function( $html, $url, $attr ) {
		if ( strpos( $html, '<embed src=' ) !== false ) {
			$html = str_replace(
				'</param><embed',
				'</param><param name=" " value="transparent"></param><embed wmode="transparent" ',
				$html
			);
		} elseif ( strpos( $html, 'feature=oembed' ) !== false ) {
			$html = str_replace(
				'feature=oembed',
				'feature=oembed&wmode=transparent&showinfo=0&autohide=1',
				$html
			);
		} elseif ( strpos( $html, 'vimeo.com' ) !== false ) {
			$html = str_replace(
				'" width=',
				'?portrait=0&badge=0&byline=0&title=0&color=999999" width=',
				$html
			);
		}

		return sprintf( '<figure class="wp-embed">%s</figure>', $html );
	},
	10,
	3
);
